<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>

	<div class="top-bar">
    <div class="top-bar-left">
      <ul class="dropdown menu" data-dropdown-menu>
        <li>
          <a href="index.jsp">Principal</a>
        </li>

        <li>
          <a href="#">Novo</a>
          <ul class="menu vertical">
            <li><a href="CadastroCliente.jsp">Cliente</a></li>
            <li><a href="CadastroProduto.jsp">Produto</a></li>
            <li><a href="CadastroFornecedor.jsp">Fornecedor</a></li>
            <li><a href="CadastroNotaDeVenda.jsp">Nota de venda</a></li>
            <li><a href="CadastroNotaDeCompra.jsp">Nota de compra</a></li>
          </ul>
        </li>

        <li>
          <a href="Pesquisa.jsp">Pesquisar</a>
        </li>
      </ul>
    </div>
	</div>

</html>
