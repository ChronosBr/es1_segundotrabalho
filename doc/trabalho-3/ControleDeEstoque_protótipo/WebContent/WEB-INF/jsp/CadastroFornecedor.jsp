<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<!--suppress HtmlFormInputWithoutLabel -->
<html class="no-js" lang="en" dir="ltr">

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Sistema de Controle de Estoque</title>
		<link rel="stylesheet" href="css/foundation.css">
		<link rel="stylesheet" href="css/app.css">
		<script src="js/vendor/jquery.js"></script>
		<script src="js/vendor/jquery.maskedinput.js"></script>
		<script src="js/validarcpf.js"></script>
		<script src="js/mascaras.js"></script>
	</head>

	<body>
		<jsp:include page="/biblioteca/menu.jsp"/><br>

		<div class="row medium-8 large-7 columns borda">
			<div class="blog-post">
				<h3 class="titulo">Cadastro de Fornecedor</h3>

				<form action="" method="post" autocomplete="on">
					<div id="pessoaJuridica" class="group">
						<div class="row">
							<div class="large-6 medium-6 columns">
								<label class="titulo">Razão Social</label>
								<input name="razaoSocial" type="text" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+" />
							</div>
							<div class="large-6 medium-6 columns">
								<label class="titulo">Nome Fantasia</label>
								<input name="nomeFantasia" type="text" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+" />
							</div>
						</div>
						<div class="row">
							<div class="large-4 medium-4 columns">
								<label class="titulo">CNPJ</label>
								<input id="cnpj" name="cnpj" type="text" pattern="[0-9]{2}[\.][0-9]{4}[\.][0-9]{3}[\/][0-9]{4}[\-][0-9]{2}"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="large-4 medium-4 columns">
							<label class="titulo">País</label>
							<select id="tipoCadastro">
								<option>Brasil</option>
							</select>
						</div>
						<div class="large-4 medium-4 columns">
							<label class="titulo">Estado</label>
							<select>
								<option>Paraná</option>
							</select>
						</div>
						<div class="large-4 medium-4 columns">
							<label class="titulo">Cidade</label>
							<select>
								<option>Foz do Iguaçu</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="large-4 medium-4 columns">
							<label class="titulo">Bairro</label>
							<input name="nomeBairro" type="text" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+"/>
						</div>
						<div class="large-4 medium-4 columns">
							<label class="titulo">Tipo do logradouro</label>
							<select>
								<option>Rua</option>
								<option>Avenida</option>
								<option>Alameda</option>
							</select>
						</div>
						<div class="large-4 medium-4 columns">
							<label class="titulo">Logradouro</label>
							<input name="nomeLogradouro" type="text" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+"/>
						</div>
					</div>
					<div class="row">
						<div class="large-2 medium-2 columns">
							<label class="titulo">CEP</label>
							<input id="cep" name="cep" type="text" pattern="[0-9]{4}[\-][0-9]{3}"/>
						</div>
						<div class="large-2 medium-2 columns">
							<label class="titulo">Nº endereço</label>
							<input name="endereco" type="number" min="1" />
						</div>
						<div class="large-8 medium-8 columns">
							<label class="titulo">Complemento</label>
							<input name="complemento" type="text" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+"/>
						</div>
					</div>

					<input class="button" type="submit" value="Cadastrar"/>
					<a href="index.jsp" class="secondary button">Voltar</a><br>
				</form>

			</div>
		</div>

		<script src="js/vendor/what-input.js"></script>
		<script src="js/vendor/foundation.js"></script>
		<script src="js/app.js"></script>
	</body>

</html>
