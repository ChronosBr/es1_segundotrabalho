<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<!--suppress HtmlFormInputWithoutLabel -->
<html class="no-js" lang="en" dir="ltr">

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Sistema de Controle de Estoque</title>
		<link rel="stylesheet" href="css/foundation.css">
		<link rel="stylesheet" href="css/app.css">
		<script src="js/vendor/jquery.js"></script>
		<script src="js/vendor/moment.js"></script>
		<script src="js/vendor/combodate.js"></script>
	</head>

	<body>

		<form action="" method="post" autocomplete="on">
			<div class="row">
				<div class="large-6 medium-6 columns">
					<label class="titulo">Número da nota</label>
					<input name="numeroNota" type="number" min="1"/>
				</div>
				<script>
					$(function(){
						$('#dataEmissaoNota').combodate();
					});
				</script>
				<div class="large-6 medium-6 columns">
					<label class="titulo">Data de emissão</label>
					<input id="dataEmissaoNota" data-format="DD-MM-YYYY" data-template="D MMM YYYY" name="dataEmissaoNota"
                 value="13-09-2016" type="text">
				</div>
			</div>

      <label class="titulo">Produtos</label>
      <table>
        <tr>
          <%--<td>Category</td>--%>
          <td>Produtos disponíveis</td>
          <td></td>
          <td>Produtos selecionados</td>
        </tr>
        <tr>
          <%--<td>--%>
            <%--<div>--%>
              <%--<select name="drop1" id="SelectBox" size="4" style="width:100%;height : 200px"></select>--%>
            <%--</div>--%>
          <%--</td>--%>
          <td>
            <select name="drop1" id="SelectBox2" size="4" multiple="multiple" style="width:100%;height : 200px">
              <option value="1">Produto 1</option>
              <option value="2">Produto 2</option>
              <option value="3">Produto 3</option>
              <option value="4">Produto 4</option>
            </select>
          </td>
          <td>
            <input type="button" id="add" value=">>" style="display:block;" />
            <input type="button" id="remove" value="<<" style="display:block;" />
          </td>
          <td>
            <select id="SelectedItems" size="4" multiple="multiple" style="width:100%;height : 200px"></select>
          </td>
        </tr>
        <tr>
          <%--<td></td>--%>
          <td>
            <input type="button" value="Selecionar todos" id="selectAll" style="display:block" />
            <input type="button" value="Desselecionar todos" id="deselectAll" style="display:block" />
          </td>
          <td></td>
          <td>
            <input type="button" value="Selecionar todos" id="selectAll1" style="display:block" />
            <input type="button" value="Desselecionar todos" id="deselectAll1" style="display:block" />
          </td>
        </tr>
      </table>
      <input type="hidden" id="hidden1" />
      <p id="selectedValues"></p>

      <script type="text/javascript">
        $(function () {
          var artItems = ["Art 1", "Art 2", "Art 3", "Art 4", "Art 5", "Art 6"];
          var vidItems = ["Video 1", "Video 2", "Video 3", "Video 4", "Video 5", "Video 6"];
          $('#SelectBox').change(function () {
            var str = "",
                inHTML = "",
                items;
            items = $(this).val() == 'art' ? artItems : vidItems;
            $.each(items, function (i, ob) {
              inHTML += '<option value="' + i + '">' + ob + '</option>';
            });
            $("#SelectBox2").empty().append(inHTML);
          });

          $('#SelectBox2').change(function () {
//            $("#selectedValues").text($(this).val() + ';' + $("#SelectBox").val());
            $('#hidden1').val($(this).val());
          });

          $('#add').click(function () {
            inHTML = "";
            $("#SelectBox2 option:selected").each(function () {
              inHTML += '<option value="' + $(this).val() + '">' + $(this).text() + '</option>';
            });
            $("#SelectedItems").append(inHTML);
          });
        });
        $('#remove').click(function () {
          $("#SelectedItems option:selected").remove();
        });
        $('#selectAll').click(function () {
          $('#SelectBox2 option').not(':selected').each(function () {
            $(this).attr('selected', true);
          });

          $('#SelectBox2').focus();
        });
        $('#deselectAll').click(function () {
          $('#SelectBox2 option:selected').each(function () {
            $(this).removeAttr('selected');
          });

          $('#SelectBox2').focus();
        });

        $('#selectAll1').click(function () {
          $('#SelectedItems option').not(':selected').each(function () {
            $(this).attr('selected', true);
          });

          $('#SelectedItems').focus();
        });
        $('#deselectAll1').click(function () {
          $('#SelectedItems option:selected').each(function () {
            $(this).removeAttr('selected');
          });

          $('#SelectedItems').focus();
        });
      </script>

			<label class="titulo">Fornecedor principal</label>
			<select>
				<option>Fornecedor 1</option>
				<option>Fornecedor 2</option>
        <option>Fornecedor 3</option>
			</select>
			<div class="row">
				<div class="small-4 columns">
					<label class="titulo">Valor total da nota</label>
					<div class="input-group">
						<span class="input-group-label">R$</span>
						<input class="input-group-field" name="totalNota" type="number" min="0" step="any"/>
					</div>
				</div>
				<div class="small-4 columns">
					<label class="titulo">Desconto total</label>
					<div class="input-group">
						<span class="input-group-label">R$</span>
						<input class="input-group-field" name="descontoTotalNota" type="number" min="0" step="any"/>
					</div>
				</div>
				<div class="small-4 columns">
					<label class="titulo">Valor líquido</label>
					<div class="input-group">
						<span class="input-group-label">R$</span>
						<input class="input-group-field" name="valorLiquidoNota" type="number" min="0" step="any"/>
					</div>
				</div>
			</div>

			<input class="button" type="submit" value="Cadastrar"/>
			<a href="index.jsp" class="secondary button">Voltar</a><br>
		</form>

		<script src="js/vendor/what-input.js"></script>
		<script src="js/vendor/foundation.js"></script>
		<script src="js/app.js"></script>
	</body>

</html>
