<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<!--suppress HtmlFormInputWithoutLabel -->
<html class="no-js" lang="en" dir="ltr">

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Sistema de Controle de Estoque</title>
		<link rel="stylesheet" href="css/foundation.css">
		<link rel="stylesheet" href="css/app.css">
		<script src="js/vendor/jquery.js"></script>
	</head>

	<body>
		<jsp:include page="/biblioteca/menu.jsp"/><br>

		<div class="row medium-8 large-7 columns borda">
			<div class="blog-post">
				<h3 class="titulo">Cadastro de Produto</h3>

				<form action="" method="post" autocomplete="on">
					<div class="row">
						<div class="large-6 medium-6 columns">
							<label class="titulo">Código do produto</label>
							<input name="codigoProduto" type="number" min="1"/>
						</div>
						<div class="large-6 medium-6 columns">
							<label class="titulo">Código de barras</label>
							<input name="codigoBarras" type="number" min="1"/>
						</div>
					</div>
					<label class="titulo">Nome do produto</label>
					<input name="nomeProduto" type="text" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+"/>
					<label class="titulo">Tipo do produto</label>
          <div class="row">
            <div class="large-6 medium-6 columns">
              <select id="tipoProduto">
                <option>Eletrônico</option>
                <option>Eletrodoméstico</option>
                <option value="outroTipoProduto">Outro...</option>
              </select>
            </div>
            <div id="outroTipoProduto" class="group large-6 medium-6 columns">
              <input name="tipoProduto" type="text" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+"/>
            </div>
          </div>
					<label class="titulo">Fornecedor principal</label>
					<input name="fornecedor" type="text" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+"/>
					<div class="row">
						<div class="large-6 medium-6 columns">
							<label class="titulo">Preço de custo atual</label>
							<input name="precoCustoAtual" type="number" min="0" step="any"/> 
						</div>
						<div class="large-6 medium-6 columns">
							<label class="titulo">Preço de venda</label>
							<input name="precoVenda" type="number" min="0" step="any"/> 
						</div>
					</div>
					<div class="row">
						<div class="large-6 medium-6 columns">
							<label class="titulo">Quantidade atual em estoque</label>
							<input name="qtdAtual" type="number" min="0"/> 
						</div>
						<div class="large-6 medium-6 columns">
							<label class="titulo">Imagem do produto</label>
							<img src="http://www.flavorsys.com.br/img/Produtos.png" /><br>
              <label for="exampleFileUpload" class="button">Enviar arquivo</label>
              <input type="file" id="exampleFileUpload" class="show-for-sr">
						</div>
					</div>

					<input class="button" type="submit" value="Cadastrar"/>
					<a href="index.jsp" class="secondary button">Voltar</a><br>
				</form>

				<script>
					$(document).ready(function () {
						$('.group').hide();
						$('#outroTipoProduto').hide();
						$('#tipoProduto').change(function () {
							$('.group').hide();
							$('#'+$(this).val()).show();
						})
					});
				</script>

			</div>
		</div>

		<script src="js/vendor/what-input.js"></script>
		<script src="js/vendor/foundation.js"></script>
		<script src="js/app.js"></script>
	</body>

</html>
