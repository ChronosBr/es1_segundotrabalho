<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<!--suppress HtmlFormInputWithoutLabel -->
<html class="no-js" lang="en" dir="ltr">

  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistema de Controle de Estoque</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/moment.js"></script>
    <script src="js/vendor/combodate.js"></script>
  </head>

  <body>
    <jsp:include page="/biblioteca/menu.jsp"/><br>
    <div class="row medium-8 large-7 columns borda">
      <div class="blog-post">
        <h3 class="titulo">Cadastro de Nota de Compra</h3>
        <jsp:include page="CadastroNota.jsp"/>
      </div>
    </div>
  </body>

</html>

