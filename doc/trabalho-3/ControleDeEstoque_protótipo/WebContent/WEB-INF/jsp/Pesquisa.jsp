<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<!--suppress HtmlFormInputWithoutLabel -->
<html class="no-js" lang="en" dir="ltr">

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Sistema de Controle de Estoque</title>
		<link rel="stylesheet" href="css/foundation.css">
		<link rel="stylesheet" href="css/app.css">
		<script src="js/vendor/jquery.js"></script>
	</head>

	<body>
		<jsp:include page="/biblioteca/menu.jsp"/><br>
		<div class="row">
			<div class="blog-post">

				<h3 class="titulo">Pesquisa </h3>

				<form action="" method="post" autocomplete="on">
					<label class="titulo">Selecione o que deseja pesquisar</label>
					<div class="row">
						<div class="large-2 medium-2 columns">
							<select>
								<option value="cliente">Cliente</option>
								<option value="produto">Produto</option>
								<option value="fornecedor">Fornecedor</option>
								<option value="notaCompra">Nota de compra</option>
								<option value="notaVenda">Nota de venda</option>
							</select>
						</div>
					</div>

					<input name="campoPesquisa" type="search"/>

					<%-- <input class="button" type="submit" value="Pesquisar"/><br> --%>
					<a class="button" onclick="mostrarDivTabela();" href="#">Pesquisar</a>
				</form>

				<div id="divTabela" style="display:none;">
					<hr>
					<h4 class="titulo">Resultados da Pesquisa </h4>
					<table style="width:100%">
						<thead>
							<tr>
								<th style="text-align: center">Dado 1</th>
								<th style="text-align: center">Dado 2</th>
								<th style="text-align: center">Operações</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Nome 1</td>
								<td>
									<div class="small button-group">
										<a data-open="detalhes" class="button">Detalhes</a>
										<a class="success button" onclick="mostrarDivEditar();" href="#">Editar</a>
										<a data-open="excluir" class="alert button">Excluir</a>
									</div>
								</td>
							</tr>
							<tr>
								<td>2</td>
								<td>Nome 2</td>
								<td>
                  <div class="small button-group">
                    <a data-open="detalhes" class="button">Detalhes</a>
                    <a class="success button">Editar</a>
                    <a data-open="excluir" class="alert button">Excluir</a>
                  </div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>

        <div id="divEditar" style="display:none;">
          <hr>
          <h4 class="titulo">Editar</h4>
          <div class="row">
            <div class="small-4 columns">
              <label class="titulo">Atributo 1</label>
              <input type="text" value="Dado atual"/>
            </div>
            <div class="small-4 columns">
              <label class="titulo">Atributo 2</label>
              <input type="text" value="Dado atual"/>
            </div>
            <div class="small-4 columns">
              <label class="titulo">Atributo 3</label>
              <input type="text" value="Dado atual"/>
            </div>
          </div>
          <input class="button" type="submit" value="Salvar"/>
          <a class="secondary button" onclick="fecharDivEditar();" href="#">Cancelar</a>
        </div>

				<div class="reveal" id="detalhes" data-reveal>
					<button class="close-button" data-close aria-label="Close reveal" type="button">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4>Item 1</h4>
					Dado 1: 1<br>
					Dado 2: Nome 1<br> 
					Dado 3: ...<br> 
					Dado 4: ...<br>
					Dado 5: ...<br>
				</div>

				<div class="reveal" id="excluir" data-reveal>
					<button class="close-button" data-close aria-label="Close reveal" type="button">
						<span aria-hidden="true">&times;</span>
					</button>
					<div class="center">
						Deseja realmente excluir?<br><br>
						<a href="#" class="button">Sim</a>
						<a href="#" class="secondary button" data-close>Não</a>
					</div>
				</div>

        <script type="text/javascript">
          function mostrarDivTabela() {
            document.getElementById('divTabela').style.display = "block";
          }
          function mostrarDivEditar() {
            document.getElementById('divEditar').style.display = "block";
          }
          function fecharDivEditar() {
            document.getElementById('divEditar').style.display = "none";
          }
        </script>

			</div>
		</div>

		<script src="js/vendor/what-input.js"></script>
		<script src="js/vendor/foundation.js"></script>
		<script src="js/app.js"></script>
	</body>

</html>
