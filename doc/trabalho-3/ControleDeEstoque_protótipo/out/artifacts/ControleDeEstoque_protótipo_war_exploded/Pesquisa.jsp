<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Serviços de Endereço</title>
		<link rel="stylesheet" href="css/foundation.css">
		<link rel="stylesheet" href="css/app.css">
		<script src="js/vendor/jquery.js"></script>
	</head>

	<body>
		<jsp:include page="/Biblioteca/Menu.jsp"/><br>
		<div class="row medium-8 large-7 columns borda">
			<div class="blog-post">

				<h2
					class="TituloPagina center">Pesquisa
				</h2>

				<form action="" method="post" autocomplete="on">
					<label class="textoTitulo">Selection o que quer pesquisar:</label>
					<select>
						<option value="cliente">Cliente</option>
						<option value="produto">Produto</option>
						<option value="fornecedor">Fornecedor</option>
						<option value="notaCompra">Nota de compra</option>
						<option value="notaVenda">Nota de venda</option>
					</select>

					<input name="campoPesquisa" type="text"/>

					<input class="al" type="submit" value="Pesquisar"/><br><br>
				</form>

			</div>
		</div>

		<script src="js/vendor/what-input.js"></script>
		<script src="js/vendor/foundation.js"></script>
		<script src="js/app.js"></script>
	</body>

</html>
