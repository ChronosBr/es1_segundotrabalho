<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Serviços de Endereço</title>
		<link rel="stylesheet" href="css/foundation.css">
		<link rel="stylesheet" href="css/app.css">
		<script src="js/vendor/jquery.js"></script>
	</head>

	<body>
		<jsp:include page="/Biblioteca/Menu.jsp"/><br>
		<div class="row medium-8 large-7 columns borda">
			<div class="blog-post">

				<h2
					class="TituloPagina center">Cadastro de endereço
				</h2>

				<form action="CadastroEndereco" method="post" autocomplete="on">
                    <%-- TODO: combobox pra escolher o tipo de logradouro? Campo de sigla pra país e UF? --%>
                    <label class="textoTitulo">País:</label>
                    <input name="nomePais" type="text" placeholder="Ex.: Brasil" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+"/>
                    
                    <label class="textoTitulo">Unidade Federativa:</label>
                    <input name="nomeUF" type="text" placeholder="Ex.: Paraná" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+"/>
                    
                    <label class="textoTitulo">Cidade:</label>
                    <input name="nomeCidade" type="text" placeholder="Ex.: Foz do Iguaçu" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+"/>
                    
                    <label class="textoTitulo">Bairro:</label>
                    <input name="nomeBairro" type="text" placeholder="Ex.: Conjunto B" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+"/>
                    
                    <label class="textoTitulo">Tipo do logradouro:</label>
                    <input name="nomeTipoLogradouro" type="text" placeholder="Ex.: Avenida" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+"/>
                    
					<label class="textoTitulo">Logradouro:</label>
					<input name="nomeLogradouro" type="text" placeholder="Ex.: Tancredo Neves" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+"/>
                    
                    <label class="textoTitulo">CEP:</label>
                    <input name="numeroCEP" type="text" placeholder="Ex.: 85867000 ou 85867-000 (8 números)" pattern="[0-9]{5}[\-]?[0-9]{3}"/>

					<input class="al" type="submit" value="Cadastrar"/><br><br>
				</form>

			</div>
		</div>

		<script src="js/vendor/what-input.js"></script>
		<script src="js/vendor/foundation.js"></script>
		<script src="js/app.js"></script>
	</body>

</html>
