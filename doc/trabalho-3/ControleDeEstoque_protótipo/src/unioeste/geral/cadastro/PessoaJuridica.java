package unioeste.geral.cadastro;

import java.io.Serializable;

public abstract class PessoaJuridica extends Pessoa implements Serializable {
	private static final long serialVersionUID = -7268043962587879190L;
	private String nomeFantasia;
	private String nomeRazaoSocial;
	private CNPJ cNPJ;
}
