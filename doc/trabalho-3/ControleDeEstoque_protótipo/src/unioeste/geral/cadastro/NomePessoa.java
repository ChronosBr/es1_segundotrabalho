package unioeste.geral.cadastro;

import java.io.Serializable;

public class NomePessoa implements Serializable {
	private static final long serialVersionUID = 8962777884993590345L;
	private String primeiroNome;
	private String nomeDoMeio;
	private String ultimoNome;
	private String nomeCompleto;
}
