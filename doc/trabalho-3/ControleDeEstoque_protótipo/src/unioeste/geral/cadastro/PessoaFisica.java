package unioeste.geral.cadastro;

import java.io.Serializable;

public abstract class PessoaFisica extends Pessoa implements Serializable {
	private static final long serialVersionUID = 9141369442101178846L;
	private Sexo sexo;
	private CPF cPF;
	private NomePessoa nomePessoa;
}
