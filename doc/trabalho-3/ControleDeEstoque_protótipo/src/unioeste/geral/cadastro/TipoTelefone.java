package unioeste.geral.cadastro;

import java.io.Serializable;

public class TipoTelefone implements Serializable {
	private static final long serialVersionUID = -3290465431277390233L;
	private String nomeTipoTelefone;
	private int idTipoTelefone;
	private DDD dDD;
}
