package unioeste.geral.produto;

import unioeste.geral.cadastro.Fornecedor;

import java.io.Serializable;

public class Produto implements Serializable {
	private static final long serialVersionUID = -7297468868484857735L;
	private int idProduto;
	private String codigoBarrasProduto;
	private String nomeProduto;
	private double precoCustoProduto;
	private double precoVendaProduto;
	private int qtdProduto;
	private TipoProduto tipoProduto;
	private Fornecedor fornecedor;
}
