package unioeste.geral.nota;

import java.io.Serializable;
import java.util.Date;

public abstract class Nota implements Serializable {
	private static final long serialVersionUID = -6301623623599450399L;
	private int nroNota;
	private Date dataEmissaoNota;
	private double totalBrutoNota;
	private double descontoTotalNota;
	private double valorLiquidoNota;
    // TODO
//	private ItemNota itemNota;
//	private EnderecoEspecifico enderecoEspecifico;
//	private ItemNota[] itemNota;
}
