package unioeste.geral.itemNota;

import java.io.Serializable;

public class FormaPagamento implements Serializable {
	private static final long serialVersionUID = 3713746589817379366L;
	private String nomeFormaPagamento;
	private int idFormaPagamento;
	private ItemNotaVenda itemNotaVenda;
}
