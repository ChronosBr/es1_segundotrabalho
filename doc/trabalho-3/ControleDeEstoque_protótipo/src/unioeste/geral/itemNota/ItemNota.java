package unioeste.geral.itemNota;

import unioeste.geral.produto.Produto;

import java.io.Serializable;

public abstract class ItemNota implements Serializable {
	private static final long serialVersionUID = 6384103101245341744L;
	private int idItemNota;
	private double precoUnitario;
	private int qtdItemNota;
	private double totalItemNota;
	private Produto produto;
}
