package servlet;

import unioeste.geral.bo.endereco.Endereco;
import unioeste.geral.endereco.exception.EnderecoException;
import unioeste.geral.endereco.manager.EnderecoControle;
import unioeste.geral.endereco.manager.UCEnderecoGeralServicos;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/CadastroEnderecoServlet")
public class CadastroEnderecoServlet extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher;
        request.setCharacterEncoding("UTF-8"); // pega caracteres UTF-8 do JSP
        request.setAttribute("pagina", "CadastroEndereco.jsp");

        String nomeTipoLogradouro = request.getParameter("nomeTipoLogradouro");
        String nomeLogradouro = request.getParameter("nomeLogradouro");
        String nomeBairro = request.getParameter("nomeBairro");
        String nomeCidade = request.getParameter("nomeCidade");
        String nomeUF = request.getParameter("nomeUF");
        String siglaUF = request.getParameter("siglaUF");
        String nomePais = request.getParameter("nomePais");
        String siglaPais = request.getParameter("siglaPais");
        String cep = request.getParameter("cep");

        Endereco endereco = new EnderecoControle().popularEndereco(nomeTipoLogradouro, nomeLogradouro, nomeBairro,
                nomeCidade, nomeUF, siglaUF, nomePais, siglaPais, cep);

        try {
            new UCEnderecoGeralServicos().cadastrarEndereco(endereco);
            requestDispatcher = request.getRequestDispatcher("Sucesso.jsp");
        } catch (EnderecoException e) {
            request.setAttribute("e", e);
            requestDispatcher = request.getRequestDispatcher("Falha.jsp");
        }
        requestDispatcher.forward(request, response);
    }


}
