<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Serviços de Endereço</title>
		<link rel="stylesheet" href="css/foundation.css">
		<link rel="stylesheet" href="css/app.css">
		<script src="js/vendor/jquery.js"></script>
	</head>

	<body>
		<jsp:include page="/Biblioteca/Menu.jsp"/>

    <br>
		<div class="row medium-8 large-7 columns borda">
			<div class="blog-post">

				<h3
					class="TituloPagina">Cadastro de endereço
				</h3>

        <form action="CadastroEnderecoServlet" method="post" autocomplete="on">
          <div class="row">
            <div class="large-10 medium-10 columns">
              <label>
                <strong>País</strong>
                <input name="nomePais" type="text" pattern="[0-9a-zA-ZÀ-ÿ\-'ºª ]+" required/>
              </label>
							<p class="help-text">Ex.: Brasil</p>
            </div>
            <div class="large-2 medium-2 columns">
              <label>
                <strong>Sigla</strong>
                <input name="siglaPais" type="text" pattern="[a-zA-Z]{2,3}" required/>
              </label>
              <p class="help-text">Ex.: BR</p>
            </div>
          </div>

          <div class="row">
            <div class="large-10 medium-10 columns">
              <label>
                <strong>Unidade Federativa</strong>
                <input name="nomeUF" type="text" pattern="[0-9a-zA-ZÀ-ÿ\-'ºª ]+" required/>
              </label>
							<p class="help-text">Ex.: Paraná</p>
            </div>
            <div class="large-2 medium-2 columns">
              <label>
                <strong>Sigla</strong>
                <input name="siglaUF" type="text" pattern="[a-zA-Z]{2,3}" required/>
              </label>
							<p class="help-text">Ex.: PR</p>
            </div>
          </div>

          <label>
            <strong>Cidade</strong>
            <input name="nomeCidade" type="text" pattern="[0-9a-zA-ZÀ-ÿ\-'ºª ]+" required/>
          </label>
          <p class="help-text">Ex.: Foz do Iguaçu</p>

          <label>
            <strong>Bairro</strong>
            <input name="nomeBairro" type="text" pattern="[0-9a-zA-ZÀ-ÿ\-'ºª ]+" required/>
          </label>
          <p class="help-text">Ex.: Conjunto B</p>

          <div class="row">
            <div class="large-4 medium-4 columns">
							<label>
                <strong>Tipo do logradouro</strong>
                <input name="nomeTipoLogradouro" type="text" pattern="[0-9a-zA-ZÀ-ÿ\-'ºª ]+" required/>
              </label>
							<p class="help-text">Ex.: Avenida</p>
            </div>
            <div class="large-8 medium-8 columns">
							<label>
                <strong>Logradouro</strong>
                <input name="nomeLogradouro" type="text" pattern="[0-9a-zA-ZÀ-ÿ\-'ºª ]+" required/>
              </label>
							<p class="help-text">Ex.: Tancredo Neves</p>
            </div>
					</div>

          <label>
            <strong>CEP</strong>
            <input name="cep" type="text" pattern="[0-9]{5}[\-][0-9]{3}" required/>
          </label>
          <p class="help-text">Ex.: 99999-999</p>

          <input class="button" type="submit" value="Cadastrar"/>
          <a href="index.jsp" class="secondary button">Voltar</a>
          <br>
        </form>

			</div>
		</div>

		<script src="js/vendor/what-input.js"></script>
		<script src="js/vendor/foundation.js"></script>
		<script src="js/app.js"></script>
	</body>

</html>
