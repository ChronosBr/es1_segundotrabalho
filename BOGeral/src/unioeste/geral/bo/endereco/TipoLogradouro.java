package unioeste.geral.bo.endereco;

import java.io.Serializable;

public class TipoLogradouro implements Serializable {
	private int idTipoLogradouro;
	private String nomeTipoLogradouro;

    public TipoLogradouro(int idTipoLogradouro, String nomeTipoLogradouro) {
        this.idTipoLogradouro = idTipoLogradouro;
        this.nomeTipoLogradouro = nomeTipoLogradouro;
    }

    public TipoLogradouro() {}

    public TipoLogradouro(String nomeTipoLogradouro) {
        this.nomeTipoLogradouro = nomeTipoLogradouro;
    }

    public String getNomeTipoLogradouro() {
        return nomeTipoLogradouro;
    }

    public void setNomeTipoLogradouro(String nomeTipoLogradouro) {
        this.nomeTipoLogradouro = nomeTipoLogradouro;
    }

    public int getIdTipoLogradouro() {
        return idTipoLogradouro;
    }

    public void setIdTipoLogradouro(int idTipoLogradouro) {
        this.idTipoLogradouro = idTipoLogradouro;
    }
}
