package unioeste.geral.bo.endereco;

import java.io.Serializable;

public class Cidade implements Serializable{
	private int idCidade;
	private String nomeCidade;
	private UnidadeFederativa unidadeFederativa;

    public Cidade(int idCidade, String nomeCidade, UnidadeFederativa unidadeFederativa) {
        this.idCidade = idCidade;
        this.nomeCidade = nomeCidade;
        this.unidadeFederativa = unidadeFederativa;
    }

    public Cidade(String nomeCidade, UnidadeFederativa unidadeFederativa) {
        this.nomeCidade = nomeCidade;
        this.unidadeFederativa = unidadeFederativa;
    }

    public Cidade() {}

    public int getIdCidade() {
        return idCidade;
    }

    public void setIdCidade(int idCidade) {
        this.idCidade = idCidade;
    }

    public String getNomeCidade() {
        return nomeCidade;
    }

    public void setNomeCidade(String nomeCidade) {
        this.nomeCidade = nomeCidade;
    }

    public UnidadeFederativa getUnidadeFederativa() {
        return unidadeFederativa;
    }

    public void setUnidadeFederativa(UnidadeFederativa unidadeFederativa) {
        this.unidadeFederativa = unidadeFederativa;
    }
}
