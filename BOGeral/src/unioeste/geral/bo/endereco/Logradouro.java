package unioeste.geral.bo.endereco;

import java.io.Serializable;

public class Logradouro implements Serializable {
	private int idLogradouro;
	private String nomeLogradouro;
	private TipoLogradouro tipoLogradouro;

    public Logradouro(int idLogradouro, String nomeLogradouro, TipoLogradouro tipoLogradouro) {
        this.idLogradouro = idLogradouro;
        this.nomeLogradouro = nomeLogradouro;
        this.tipoLogradouro = tipoLogradouro;
    }

    public Logradouro(String nomeLogradouro, TipoLogradouro tipoLogradouro) {
        this.nomeLogradouro = nomeLogradouro;
        this.tipoLogradouro = tipoLogradouro;
    }

    public Logradouro() {}

    public int getIdLogradouro() {
        return idLogradouro;
    }

    public void setIdLogradouro(int idLogradouro) {
        this.idLogradouro = idLogradouro;
    }

    public String getNomeLogradouro() {
        return nomeLogradouro;
    }

    public void setNomeLogradouro(String nomeLogradouro) {
        this.nomeLogradouro = nomeLogradouro;
    }

    public TipoLogradouro getTipoLogradouro() {
        return tipoLogradouro;
    }

    public void setTipoLogradouro(TipoLogradouro tipoLogradouro) {
        this.tipoLogradouro = tipoLogradouro;
    }
}
