package unioeste.geral.bo.endereco;

import java.io.Serializable;

public class Bairro implements Serializable {
	private int idBairro;
    private String nomeBairro;

    public Bairro(int idBairro, String nomeBairro) {
        this.idBairro = idBairro;
        this.nomeBairro = nomeBairro;
    }

    public Bairro() {}

    public Bairro(String nomeBairro) {
        this.nomeBairro = nomeBairro;
    }

    public int getIdBairro() {
        return idBairro;
    }

    public void setIdBairro(int idBairro) {
        this.idBairro = idBairro;
    }

    public String getNomeBairro() {
        return nomeBairro;
    }

    public void setNomeBairro(String nomeBairro) {
        this.nomeBairro = nomeBairro;
    }
}
