package unioeste.geral.bo.endereco;

import java.io.Serializable;

public class UnidadeFederativa implements Serializable {
    private int idUnidadeFederativa;
	private String nomeUnidadeFederativa;
	private String siglaUnidadeFederativa;
    private Pais pais;

    public UnidadeFederativa(int idUnidadeFederativa, String nomeUnidadeFederativa, String siglaUnidadeFederativa, Pais pais) {
        this.idUnidadeFederativa = idUnidadeFederativa;
        this.nomeUnidadeFederativa = nomeUnidadeFederativa;
        this.siglaUnidadeFederativa = siglaUnidadeFederativa;
        this.pais = pais;
    }

    public UnidadeFederativa(String nomeUnidadeFederativa, String siglaUnidadeFederativa, Pais pais) {
        this.nomeUnidadeFederativa = nomeUnidadeFederativa;
        this.siglaUnidadeFederativa = siglaUnidadeFederativa;
        this.pais = pais;
    }

    public UnidadeFederativa() {}

    public int getIdUnidadeFederativa() {
        return idUnidadeFederativa;
    }

    public void setIdUnidadeFederativa(int idUnidadeFederativa) {
        this.idUnidadeFederativa = idUnidadeFederativa;
    }

    public String getNomeUnidadeFederativa() {
        return nomeUnidadeFederativa;
    }

    public void setNomeUnidadeFederativa(String nomeUnidadeFederativa) {
        this.nomeUnidadeFederativa = nomeUnidadeFederativa;
    }

    public String getSiglaUnidadeFederativa() {
        return siglaUnidadeFederativa;
    }

    public void setSiglaUnidadeFederativa(String siglaUnidadeFederativa) {
        this.siglaUnidadeFederativa = siglaUnidadeFederativa;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }
}
