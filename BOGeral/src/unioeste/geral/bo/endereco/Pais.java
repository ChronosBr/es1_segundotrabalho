package unioeste.geral.bo.endereco;

import java.io.Serializable;

public class Pais implements Serializable {
	private int idPais;
	private String nomePais;
    private String siglaPais;

    public Pais(int idPais, String nomePais, String siglaPais) {
        this.idPais = idPais;
        this.nomePais = nomePais;
        this.siglaPais = siglaPais;
    }

    public Pais(String nomePais, String siglaPais) {
        this.nomePais = nomePais;
        this.siglaPais = siglaPais;
    }

    public Pais() {}

    public int getIdPais() {
        return idPais;
    }

    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }

    public String getNomePais() {
        return nomePais;
    }

    public void setNomePais(String nomePais) {
        this.nomePais = nomePais;
    }

    public String getSiglaPais() {
        return siglaPais;
    }

    public void setSiglaPais(String siglaPais) {
        this.siglaPais = siglaPais;
    }
}
