<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Serviços de Endereço</title>
		<link rel="stylesheet" href="css/foundation.css">
		<link rel="stylesheet" href="css/app.css">
		<script src="js/vendor/jquery.js"></script>
	</head>

	<body>
		<jsp:include page="/Biblioteca/Menu.jsp"/><br>
		<div class="row columns borda">
			<div class="blog-post">

        <h3 class="tituloPagina">Pesquisar Endereço</h3>

        <div class="row">
          <div class="large-2 medium-2 columns">
            <label>
              Busca por:
              <select id="tipoBusca">
                <option value="buscaPorCEP">CEP</option>
                <option value="buscaPorID">ID</option>
              </select>
            </label>
          </div>
        </div>

        <script>
          $(document).ready(function () {
            $('.group').hide();
            $('#buscaPorCEP').show();
            $('#tipoBusca').change(function () {
              $('.group').hide();
              $('#'+$(this).val()).show();
            })
          });
        </script>

        <form action="BuscaEnderecoServlet" method="post" autocomplete="on">
          <div id="buscaPorCEP" class="group">
            <div class="large-2 medium-2">
              <label>
                <strong>CEP</strong>
                <input name="cep" type="text" pattern="\d{5}\-\d{3}"/>
                <p class="help-text">Formato: 99999-999</p>
              </label>
              <input class="button" type="submit" value="Pesquisar"/><br>
            </div>
          </div>

          <div id="buscaPorID" class="group">
            <div class="large-2 medium-2">
              <label>
                <strong>ID</strong>
                <input name="id" type="number" min="1"/>
                <p class="help-text">Número >= 1</p>
              </label>
              <input class="button" type="submit" value="Pesquisar"/><br>
            </div>
          </div>
        </form>

			</div>
		</div>

		<script src="js/vendor/what-input.js"></script>
		<script src="js/vendor/foundation.js"></script>
		<script src="js/app.js"></script>
	</body>

</html>