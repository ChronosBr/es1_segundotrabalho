package unioeste.geral.endereco.manager;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.endereco.Pais;
import unioeste.geral.endereco.dao.PaisDAO;
import unioeste.geral.endereco.exception.EnderecoException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
// to do Adicionar os throws 
public class PaisControle {
	private final Connection connection;
    private static final String USUARIO_BD = "root";
    private static final String SENHA_BD   = "";
    private static final String URL_BD     = "jdbc:mysql://localhost/endereco";

	public PaisControle() {
        this.connection = new Conector().getConnection(URL_BD, USUARIO_BD, SENHA_BD);
	}

	public void salvarPais(Pais pais) {
        try {
            PaisDAO paisDAO = new PaisDAO();
            paisDAO.inserirPais(pais, connection);
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}

    public ArrayList<Pais> obterListaPais() {
    	ArrayList<Pais> lista= new ArrayList<>();
    	Pais pais = new Pais();
    	PaisDAO paisDao = new PaisDAO();
        ResultSet rs = null;
        try {
            rs = paisDao.buscarTodosPais(connection);
        } catch (EnderecoException e) {
            e.printStackTrace();
        }
        if (rs != null) {
            try {
                while(rs.next()){
                	pais = new Pais(rs.getInt("idPais"), rs.getString("nomePais"), rs.getString("siglaPais"));
                	lista.add(pais);
                }
                connection.close();
                return lista;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
		return null;
    }
    
    public ArrayList<String> NomePaises(ArrayList<Pais> paises){
    	ArrayList<String> nomes = new ArrayList<String>();
    	for(int i=0; i<paises.size();i++){
    		nomes.add(paises.get(i).getNomePais());
    	}
    	return nomes;
    }
    public ArrayList<Integer> PaisesId(ArrayList<Pais> paises){
    	ArrayList<Integer> nomes = new ArrayList<Integer>();
    	for(int i=0; i<paises.size();i++){
    		nomes.add(paises.get(i).getIdPais());
    	}
    	return nomes;
    }
    
    public ArrayList<String> PaisesNomeEId(ArrayList<Pais> paises){
    	ArrayList<String> nomes = new ArrayList<String>();
    	for(int i=0; i<paises.size();i++){
                nomes.add(paises.get(i).getNomePais());
    		nomes.add(Integer.toString(paises.get(i).getIdPais()));
    	}
    	return nomes;
    }
    
    public Pais obterPais(int idPais) {
        PaisDAO bairroDAO = new PaisDAO();
        ResultSet rs = null;
        try {
            rs = bairroDAO.buscarPaisPorID(idPais, connection);
        } catch (EnderecoException e) {
            e.printStackTrace();
        }
        if (rs != null) {
            try {
                rs.next();
                Pais bairro = new Pais(rs.getInt("idPais"), rs.getString("nomePais"), rs.getString("siglaPais"));
                connection.close();
                return bairro;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
