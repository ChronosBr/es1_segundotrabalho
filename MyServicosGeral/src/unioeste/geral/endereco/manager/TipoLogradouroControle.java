package unioeste.geral.endereco.manager;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.endereco.TipoLogradouro;
import unioeste.geral.endereco.dao.TipoLogradouroDAO;
import unioeste.geral.endereco.exception.EnderecoException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TipoLogradouroControle {
	private final Connection connection;
    private static final String USUARIO_BD = "root";
    private static final String SENHA_BD   = "";
    private static final String URL_BD     = "jdbc:mysql://localhost/endereco";

	public TipoLogradouroControle() {
        this.connection = new Conector().getConnection(URL_BD, USUARIO_BD, SENHA_BD);
	}

	public void salvarTipoLogradouro(TipoLogradouro tipoLogradouro) {
        try {
            TipoLogradouroDAO tipoLogradouroDAO = new TipoLogradouroDAO();
            tipoLogradouroDAO.inserirTipoLogradouro(tipoLogradouro, connection);
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}

    public TipoLogradouro obterTipoLogradouro(int idTipoLogradouro) {
        TipoLogradouroDAO bairroDAO = new TipoLogradouroDAO();
        ResultSet rs = null;
        try {
            rs = bairroDAO.buscarTipoLogradouroPorID(idTipoLogradouro, connection);
        } catch (EnderecoException e) {
            e.printStackTrace();
        }
        if (rs != null) {
            try {
                rs.next();
                TipoLogradouro bairro = new TipoLogradouro(rs.getInt("idTipoLogradouro"), rs.getString("nomeTipoLogradouro"));
                connection.close();
                return bairro;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
     public ArrayList<TipoLogradouro> obterListaTipoLogradouro() {
    	ArrayList<TipoLogradouro> lista= new ArrayList<>();
    	TipoLogradouroDAO logradouro = new TipoLogradouroDAO();
        ResultSet rs = null;
        try {
            rs = logradouro.buscarTipoLogradouro(connection);
        } catch (EnderecoException e) {
            e.printStackTrace();
        }
        if (rs != null) {
            try {
                while(rs.next()){
                    TipoLogradouro tipoLogradouro = new TipoLogradouro(rs.getInt("idTipoLogradouro"), rs.getString("nomeTipoLogradouro"));
                    lista.add(tipoLogradouro);
                }
                connection.close();
                return lista;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
		return null;
    }
     
     public ArrayList<String> TipoLogradouroLista(){
        ArrayList<TipoLogradouro> uf= obterListaTipoLogradouro(); 
    	ArrayList<String> nomes = new ArrayList<String>();
    	for(int i=0; i<uf.size();i++){
                nomes.add(uf.get(i).getNomeTipoLogradouro());
    		nomes.add(Integer.toString(uf.get(i).getIdTipoLogradouro()));
    	}
    	return nomes;
    }

}
