package unioeste.geral.endereco.manager;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.endereco.Cidade;
import unioeste.geral.bo.endereco.UnidadeFederativa;
import unioeste.geral.endereco.dao.CidadeDAO;
import unioeste.geral.endereco.exception.EnderecoException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CidadeControle {
    private final Connection connection;
    private static final String USUARIO_BD = "root";
    private static final String SENHA_BD   = "";
    private static final String URL_BD     = "jdbc:mysql://localhost/endereco";

	public CidadeControle() {
		this.connection = new Conector().getConnection(URL_BD, USUARIO_BD, SENHA_BD);
	}

	public void salvarCidade(Cidade cidade) {
        try {
            CidadeDAO cidadeDAO = new CidadeDAO();
            cidadeDAO.inserirCidade(cidade, connection);
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}

    public Cidade obterCidade(int idCidade) {
        CidadeDAO bairroDAO = new CidadeDAO();
        ResultSet rs = null;
        try {
            rs = bairroDAO.buscarCidadePorID(idCidade, connection);
        } catch (EnderecoException e) {
            e.printStackTrace();
        }
        if (rs != null) {
            try {
                rs.next();
                UnidadeFederativa unidadeFederativa = new UnidadeFederativaControle().obterUnidadeFederativa(
                        rs.getString("UnidadeFederativa_idUnidadeFederativa"));
                Cidade bairro = new Cidade(rs.getInt("idCidade"), rs.getString("nomeCidade"), unidadeFederativa);
                connection.close();
                return bairro;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    public ArrayList<Cidade> obterListaCidade() {
    	ArrayList<Cidade> lista= new ArrayList<>();
    	CidadeDAO cidadeDao = new CidadeDAO();
        ResultSet rs = null;
        try {
            rs = cidadeDao.buscarCidade(connection);
        } catch (EnderecoException e) {
            e.printStackTrace();
        }
        if (rs != null) {
            try {
                while(rs.next()){
                    UnidadeFederativa unidadeFederativa = new UnidadeFederativaControle().obterUnidadeFederativa(
                    rs.getString("UnidadeFederativa_idUnidadeFederativa"));
                    Cidade cidadeA = new Cidade(rs.getInt("idCidade"), rs.getString("nomeCidade"), unidadeFederativa);
                    lista.add(cidadeA);
                }
                connection.close();
                return lista;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
		return null;
    }
    
    public ArrayList<String> CidadeNomeIdIdUF(){
        ArrayList<Cidade> uf= obterListaCidade(); 
    	ArrayList<String> nomes = new ArrayList<String>();
    	for(int i=0; i<uf.size();i++){
                nomes.add(uf.get(i).getNomeCidade());
    		nomes.add(Integer.toString(uf.get(i).getIdCidade()));
                nomes.add(Integer.toString(uf.get(i).getUnidadeFederativa().getIdUnidadeFederativa()));
    	}
    	return nomes;
    }
        public ArrayList<String> CidadeDoUF(int idUF){
        ArrayList<Cidade> uf= obterListaCidade(); 
    	ArrayList<String> nomes = new ArrayList<String>();
    	for(int i=0; i<uf.size();i++){
            if(uf.get(i).getUnidadeFederativa().getIdUnidadeFederativa()==idUF){
                nomes.add(uf.get(i).getNomeCidade());
    		nomes.add(Integer.toString(uf.get(i).getIdCidade()));
                nomes.add(Integer.toString(uf.get(i).getUnidadeFederativa().getIdUnidadeFederativa()));
            }
    	}
    	return nomes;
    }

}
