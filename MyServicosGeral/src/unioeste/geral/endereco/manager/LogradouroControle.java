package unioeste.geral.endereco.manager;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.endereco.Logradouro;
import unioeste.geral.bo.endereco.TipoLogradouro;
import unioeste.geral.endereco.dao.LogradouroDAO;
import unioeste.geral.endereco.exception.EnderecoException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class LogradouroControle {
	private final Connection connection;
    private static final String USUARIO_BD = "root";
    private static final String SENHA_BD   = "";
    private static final String URL_BD     = "jdbc:mysql://localhost/endereco";

    public LogradouroControle() {
        this.connection = new Conector().getConnection(URL_BD, USUARIO_BD, SENHA_BD);
	}

	public void salvarLogradouro(Logradouro logradouro) {
        try {
            LogradouroDAO logradouroDAO = new LogradouroDAO();
            logradouroDAO.inserirLogradouro(logradouro, connection);
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}

    public Logradouro obterLogradouro(int idLogradouro) {
        LogradouroDAO tipoLogradouroDAO = new LogradouroDAO();
        ResultSet rs = null;
        try {
            rs = tipoLogradouroDAO.buscarLogradouroPorID(idLogradouro, connection);
        } catch (EnderecoException e) {
            e.printStackTrace();
        }
        if (rs != null) {
            try {
                rs.next();
                TipoLogradouro tipoLogradouro = new TipoLogradouroControle().obterTipoLogradouro(
                        rs.getInt("TipoLogradouro_idTipoLogradouro"));
                Logradouro logradouro = new Logradouro(rs.getInt("idLogradouro"), rs.getString("nomeLogradouro"), tipoLogradouro);
                logradouro.setIdLogradouro(rs.getInt("idLogradouro"));
                connection.close();
                return logradouro;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    
     public ArrayList<Logradouro> obterListaLogradouro() {
    	ArrayList<Logradouro> lista= new ArrayList<>();
    	LogradouroDAO logradourodao = new LogradouroDAO();
        ResultSet rs = null;
        try {
            rs = logradourodao.buscarLogradouro(connection);
        } catch (EnderecoException e) {
            e.printStackTrace();
        }
        if (rs != null) {
            try {
                while(rs.next()){
                    TipoLogradouro tipoLogradouro = new TipoLogradouroControle().obterTipoLogradouro(
                    rs.getInt("TipoLogradouro_idTipoLogradouro"));
                    Logradouro logradouro = new Logradouro(rs.getInt("idLogradouro"), rs.getString("nomeLogradouro"), tipoLogradouro);
                    logradouro.setIdLogradouro(rs.getInt("idLogradouro"));
                    lista.add(logradouro);
                }
                connection.close();
                return lista;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
		return null;
    }
     
     public ArrayList<String> LogradouroLista(){
        ArrayList<Logradouro> logradouro= obterListaLogradouro(); 
    	ArrayList<String> nomes = new ArrayList<String>();
    	for(int i=0; i<logradouro.size();i++){
                nomes.add(logradouro.get(i).getNomeLogradouro());
    		nomes.add(Integer.toString(logradouro.get(i).getIdLogradouro()));
                nomes.add(Integer.toString(logradouro.get(i).getTipoLogradouro().getIdTipoLogradouro()));
    	}
    	return nomes;
    }
     
      public ArrayList<String> LogradouroDeTipo(int id){
        ArrayList<Logradouro> logradouro= obterListaLogradouro(); 
    	ArrayList<String> nomes = new ArrayList<String>();
    	for(int i=0; i<logradouro.size();i++){
            if(logradouro.get(i).getTipoLogradouro().getIdTipoLogradouro()==id){
                nomes.add(logradouro.get(i).getNomeLogradouro());
    		nomes.add(Integer.toString(logradouro.get(i).getIdLogradouro()));
                nomes.add(Integer.toString(logradouro.get(i).getTipoLogradouro().getIdTipoLogradouro()));
            }
    	}
    	return nomes;
    }
}
