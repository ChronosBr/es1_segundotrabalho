package unioeste.geral.endereco.manager;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.endereco.Pais;
import unioeste.geral.bo.endereco.UnidadeFederativa;
import unioeste.geral.endereco.dao.UnidadeFederativaDAO;
import unioeste.geral.endereco.exception.EnderecoException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UnidadeFederativaControle {
	private final Connection connection;
    private static final String USUARIO_BD = "root";
    private static final String SENHA_BD   = "";
    private static final String URL_BD     = "jdbc:mysql://localhost/endereco";

	public UnidadeFederativaControle() {
        this.connection = new Conector().getConnection(URL_BD, USUARIO_BD, SENHA_BD);
	}

	public void salvarUnidadeFederativa(UnidadeFederativa unidadeFederativa) {
        try {
            UnidadeFederativaDAO unidadeFederativaDAO = new UnidadeFederativaDAO();
            unidadeFederativaDAO.inserirUnidadeFederativa(unidadeFederativa, connection);
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}

    public UnidadeFederativa obterUnidadeFederativa(String idUnidadeFederativa) {
        UnidadeFederativaDAO bairroDAO = new UnidadeFederativaDAO();
        ResultSet rs = null;
        try {
            rs = bairroDAO.buscarUnidadeFederativaPorID(idUnidadeFederativa, connection);
        } catch (EnderecoException e) {
            e.printStackTrace();
        }
        if (rs != null) {
            try {
                rs.next();
                Pais pais = new PaisControle().obterPais(rs.getInt("Pais_idPais"));
                UnidadeFederativa bairro = new UnidadeFederativa(rs.getString("nomeUnidadeFederativa"),
                        rs.getString("siglaUnidadeFederativa"), pais);
                bairro.setIdUnidadeFederativa(rs.getInt("idUnidadeFederativa"));
                connection.close();
                return bairro;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
     public ArrayList<UnidadeFederativa> obterListaUnidadeFederativa() {
    	ArrayList<UnidadeFederativa> lista= new ArrayList<>();
    	UnidadeFederativaDAO ufDao = new UnidadeFederativaDAO();
        ResultSet rs = null;
        try {
            rs = ufDao.buscarUnidadeFederativa( connection);
        } catch (EnderecoException e) {
            e.printStackTrace();
        }
        if (rs != null) {
            try {
                while(rs.next()){
                    Pais pais = new PaisControle().obterPais(rs.getInt("Pais_idPais"));
                    UnidadeFederativa ufederativa = new UnidadeFederativa(rs.getString("nomeUnidadeFederativa"),
                    rs.getString("siglaUnidadeFederativa"), pais);
                    ufederativa.setIdUnidadeFederativa(rs.getInt("idUnidadeFederativa"));
                    lista.add(ufederativa);
                }
                connection.close();
                return lista;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
		return null;
    }

     public ArrayList<String> UnidadeFederativaNomeIdIdPais(){
        ArrayList<UnidadeFederativa> uf=obterListaUnidadeFederativa(); 
    	ArrayList<String> nomes = new ArrayList<String>();
    	for(int i=0; i<uf.size();i++){
                nomes.add(uf.get(i).getNomeUnidadeFederativa());
    		nomes.add(Integer.toString(uf.get(i).getIdUnidadeFederativa()));
                nomes.add(Integer.toString(uf.get(i).getPais().getIdPais()));
    	}
    	return nomes;
    }
        public ArrayList<String> UnidadeFederativaDoPais(int idPais){
        ArrayList<UnidadeFederativa> uf=obterListaUnidadeFederativa(); 
    	ArrayList<String> nomes = new ArrayList<String>();
    	for(int i=0; i<uf.size();i++){
            if(uf.get(i).getPais().getIdPais()==idPais){
                nomes.add(uf.get(i).getNomeUnidadeFederativa());
    		nomes.add(Integer.toString(uf.get(i).getIdUnidadeFederativa()));
                nomes.add(Integer.toString(uf.get(i).getPais().getIdPais()));
            }
    	}
    	return nomes;
    }
}
