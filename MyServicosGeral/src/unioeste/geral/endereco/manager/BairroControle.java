package unioeste.geral.endereco.manager;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.endereco.Bairro;
import unioeste.geral.endereco.dao.BairroDAO;
import unioeste.geral.endereco.exception.EnderecoException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class BairroControle {
    private final Connection connection;
    private static final String USUARIO_BD = "root";
    private static final String SENHA_BD   = "";
    private static final String URL_BD     = "jdbc:mysql://localhost/endereco";

    public BairroControle() {
        this.connection = new Conector().getConnection(URL_BD, USUARIO_BD, SENHA_BD);
    }

    public void salvarBairro(Bairro bairro) {
        try {
            BairroDAO bairroDAO = new BairroDAO();
            bairroDAO.inserirBairro(bairro, connection);
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Bairro obterBairro(int idBairro) {
        BairroDAO bairroDAO = new BairroDAO();
        ResultSet rs = null;
        try {
            rs = bairroDAO.buscarBairroPorID(idBairro, connection);
        } catch (EnderecoException e) {
            e.printStackTrace();
        }
        if (rs != null) {
            try {
                rs.next();
                Bairro bairro = new Bairro(rs.getInt("idBairro"), rs.getString("nomeBairro"));
                connection.close();
                return bairro;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    public ArrayList<Bairro> obterListaBairro() {
    	ArrayList<Bairro> lista= new ArrayList<>();
    	BairroDAO bairroDao = new BairroDAO();
        ResultSet rs = null;
        try {
            rs = bairroDao.buscarBairro(connection);
        } catch (EnderecoException e) {
            e.printStackTrace();
        }
        if (rs != null) {
            try {
                while(rs.next()){
                    Bairro bairroA = new Bairro(rs.getInt("idBairro"), rs.getString("nomeBairro"));
                    lista.add(bairroA);
                }
                connection.close();
                return lista;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
		return null;
    }
    
    public ArrayList<String> bairroNomeId(){
        ArrayList<Bairro> bairro= obterListaBairro(); 
    	ArrayList<String> nomes = new ArrayList<String>();
    	for(int i=0; i<bairro.size();i++){
                nomes.add(bairro.get(i).getNomeBairro());
    		nomes.add(Integer.toString(bairro.get(i).getIdBairro()));
    	}
    	return nomes;
    }

}
