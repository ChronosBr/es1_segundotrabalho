package unioeste.geral.endereco.dao;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.endereco.Endereco;
import unioeste.geral.endereco.exception.EnderecoException;

import java.sql.*;

public class EnderecoDAO {

    public void inserirEndereco(Endereco objeto, Connection connection) throws SQLException {
        String sql =
                "INSERT INTO Endereco (" +
                    "cep," +
                    "Logradouro_idLogradouro," +
                    "Bairro_idBairro," +
                    "Cidade_idCidade," +
                    "Cidade_UnidadeFederativa_idUnidadeFederativa," +
                    "Cidade_UnidadeFederativa_Pais_idPais" +
                ") VALUES (?,?,?,?,?,?)";

        PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

        int i = 1;
        stmt.setString(i++, objeto.getCep());
        stmt.setInt(i++, objeto.getLogradouro().getIdLogradouro());
        stmt.setInt(i++, objeto.getBairro().getIdBairro());
        stmt.setInt(i++, objeto.getCidade().getIdCidade());
        stmt.setInt(i++, objeto.getCidade().getUnidadeFederativa().getIdUnidadeFederativa());
        stmt.setInt(i, objeto.getCidade().getUnidadeFederativa().getPais().getIdPais());

        stmt.execute();

        ResultSet rs = stmt.getGeneratedKeys();
        rs.next();
        objeto.setIdEndereco(rs.getInt(1));
    }

	public void alterarEndereco(Endereco objeto, Connection connection) {
        try {
            String sql =
                    "UPDATE Endereco " +
					"SET " +
                        "cep = ?, " +
					"WHERE idEndereco = ?";

            PreparedStatement stmt = connection.prepareStatement(sql);

			int i = 1;
            stmt.setString(i, objeto.getCep());

            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
        
        public void alterarEnderecoCEP(String objeto, Connection connection) {
        try {
            String sql =
                    "UPDATE Endereco " +
					"SET " +
                        "cep = ?, " +                        
                        "WHERE cep = ?";

            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, objeto);

            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

	public void deletarEndereco(Endereco objeto, Connection connection) throws SQLException {
        String sql =
                "DELETE FROM Endereco " +
                "WHERE idEndereco = ?";

        PreparedStatement stmt = connection.prepareStatement(sql);

        stmt.setInt(1, objeto.getIdEndereco());

        stmt.execute();
    }

    public ResultSet buscarEnderecoPorID(int idEndereco, Connection connection) throws Exception {
        String sql =
                "SELECT * " +
                "FROM Endereco " +
                "WHERE idEndereco = ?";
        try {
            return new Conector().buscarDados(sql, connection, idEndereco);
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new Exception("ID não existente");
    }

    public ResultSet buscarEnderecoPorCEP(String cep, Connection connection) throws Exception {
        String sql =
                "SELECT * " +
                "FROM Endereco " +
                "WHERE cep = ?";
        try {
            return new Conector().buscarDados(sql, connection, cep);
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new Exception("ID não existente");
    }

    public ResultSet buscarTodosEnderecos(Connection connection) throws EnderecoException {
        String sql =
                "SELECT * " +
                "FROM Endereco";
        try {
            return new Conector().buscarDados(sql, connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new EnderecoException("Erro na busca de todos endereços");
    }
    
}
