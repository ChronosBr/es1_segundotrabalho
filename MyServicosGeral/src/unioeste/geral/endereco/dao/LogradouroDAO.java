package unioeste.geral.endereco.dao;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.endereco.Logradouro;
import unioeste.geral.endereco.exception.EnderecoException;

import java.sql.*;
import java.util.ArrayList;

public class LogradouroDAO {

    public void inserirLogradouro(Logradouro objeto, Connection connection) {
        try {
            String sql =
                    "INSERT INTO Logradouro (" +
                        "nomeLogradouro," +
                        "TipoLogradouro_idTipoLogradouro" +
                    ") VALUES (?,?)";

            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            stmt.setString(1, objeto.getNomeLogradouro());
            stmt.setInt(2, objeto.getTipoLogradouro().getIdTipoLogradouro());

            stmt.execute();

            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            objeto.setIdLogradouro(rs.getInt(1));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

	public void alterarLogradouro(Logradouro objeto, Connection connection) {
        try {
            String sql =
                    "UPDATE Logradouro " +
					"SET " +
						"nomeLogradouro = ?, " +
						"TipoLogradouro_idTipoLogradouro = ? " +
					"WHERE idLogradouro = ?;";

            PreparedStatement stmt = connection.prepareStatement(sql);

            stmt.setString(1, objeto.getNomeLogradouro());
            stmt.setInt(2, objeto.getIdLogradouro());

            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

	public void deletarLogradouro(Logradouro objeto, Connection connection) {
        try {
            String sql =
                    "DELETE FROM Logradouro " +
					"WHERE idLogradouro = ?;";

            PreparedStatement stmt = connection.prepareStatement(sql);

            stmt.setInt(1, objeto.getIdLogradouro());

            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet buscarLogradouroPorID(int idLogradouro, Connection connection) throws EnderecoException {
        String sql =
                "SELECT * " +
                "FROM Logradouro " +
                "WHERE idLogradouro = ?";

        try {
            return new Conector().buscarDados(sql, connection, idLogradouro);
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new EnderecoException("ID não existente");
    }

    public ResultSet buscarLogradouro(Connection connection) throws EnderecoException {
        String sql =
                "SELECT * " +
                "FROM Logradouro ";

        try {
            return new Conector().buscarDados(sql, connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new EnderecoException("ID não existente");
    }
    
}
