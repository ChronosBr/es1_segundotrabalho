package unioeste.geral.endereco.dao;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.endereco.Bairro;
import unioeste.geral.endereco.exception.EnderecoException;

import java.sql.*;

public class BairroDAO {

    public void inserirBairro(Bairro objeto, Connection connection) {
        try {
            String sql =
                    "INSERT INTO Bairro (" +
                        "nomeBairro" +
                    ") VALUES (?)";

            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            stmt.setString(1, objeto.getNomeBairro());

            stmt.execute();

            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            objeto.setIdBairro(rs.getInt(1));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

	public void alterarBairro(Bairro objeto, Connection connection) {
        try {
            String sql =
                    "UPDATE Bairro " +
					"SET " +
						"nomeBairro = ? " +
					"WHERE idBairro = ?";

            PreparedStatement stmt = connection.prepareStatement(sql);

			int i = 1;
            stmt.setString(i++, objeto.getNomeBairro());
            stmt.setInt(i, objeto.getIdBairro());

            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

	public void deletarBairro(Bairro objeto, Connection connection) {
        try {
            String sql =
                    "DELETE FROM Bairro " +
					"WHERE idBairro = ?";

            PreparedStatement stmt = connection.prepareStatement(sql);

            stmt.setInt(1, objeto.getIdBairro());

            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet buscarBairroPorID(int idBairro, Connection connection) throws EnderecoException {
        String sql =
                "SELECT * " +
                "FROM Bairro " +
                "WHERE idBairro = ?";
        try {
            return new Conector().buscarDados(sql, connection, idBairro);
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new EnderecoException("ID não existente");
    }
    
    public ResultSet buscarBairro(Connection connection) throws EnderecoException {
        String sql =
                "SELECT * " +
                "FROM Bairro ";
        try {
            return new Conector().buscarDados(sql, connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new EnderecoException("Não existem bairros na base de dados");
    }
}
