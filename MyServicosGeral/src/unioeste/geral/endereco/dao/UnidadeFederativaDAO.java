package unioeste.geral.endereco.dao;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.endereco.UnidadeFederativa;
import unioeste.geral.endereco.exception.EnderecoException;

import java.sql.*;

public class UnidadeFederativaDAO {

    public void inserirUnidadeFederativa(UnidadeFederativa objeto, Connection connection) {
        try {
            String sql =
                    "INSERT INTO UnidadeFederativa (" +
                        "nomeUnidadeFederativa," +
                        "siglaUnidadeFederativa," +
                        "Pais_idPais" +
                    ") VALUES (?,?,?)";

            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			int i = 1;
            stmt.setString(i++, objeto.getNomeUnidadeFederativa());
            stmt.setString(i++, objeto.getSiglaUnidadeFederativa());
            stmt.setInt(i, objeto.getPais().getIdPais());

            stmt.execute();

            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            objeto.setIdUnidadeFederativa(rs.getInt(1));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

	public void alterarUnidadeFederativa(UnidadeFederativa objeto, Connection connection) {
        try {
            String sql =
                    "UPDATE UnidadeFederativa " +
					"SET " +
                        "nomeUnidadeFederativa = ?, " +
                        "siglaUnidadeFederativa = ?, " +
                        "Pais_idPais = ? " +
					"WHERE idUnidadeFederativa = ?;";

            PreparedStatement stmt = connection.prepareStatement(sql);

			int i = 1;
            stmt.setString(i++, objeto.getNomeUnidadeFederativa());
            stmt.setString(i++, objeto.getSiglaUnidadeFederativa());
            stmt.setInt(i++, objeto.getPais().getIdPais());
            stmt.setInt(i, objeto.getIdUnidadeFederativa());

            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

	public void deletarUnidadeFederativa(UnidadeFederativa objeto, Connection connection) {
        try {
            String sql =
                    "DELETE FROM UnidadeFederativa " +
					"WHERE idUnidadeFederativa = ?;";

            PreparedStatement stmt = connection.prepareStatement(sql);

            stmt.setInt(1, objeto.getIdUnidadeFederativa());

            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet buscarUnidadeFederativaPorID(String idUnidadeFederativa, Connection connection) throws EnderecoException {
        String sql =
                "SELECT * " +
                "FROM UnidadeFederativa " +
                "WHERE idUnidadeFederativa = ?";

        try {
            return new Conector().buscarDados(sql, connection, idUnidadeFederativa);
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new EnderecoException("ID não existente");
    }

    public ResultSet buscarUnidadeFederativa(Connection connection) throws EnderecoException {
        String sql =
                "SELECT * " +
                "FROM UnidadeFederativa ";

        try {
            return new Conector().buscarDados(sql, connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new EnderecoException("ID não existente");
    }
    
}
