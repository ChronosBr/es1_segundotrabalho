package unioeste.geral.endereco.dao;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.endereco.Pais;
import unioeste.geral.endereco.exception.EnderecoException;

import java.sql.*;

public class PaisDAO {

    public void inserirPais(Pais objeto, Connection connection) {
        try {
            String sql =
                    "INSERT INTO Pais (" +
                        "nomePais," +
                        "siglaPais" +
                    ") VALUES (?,?)";

            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			int i = 1;
            stmt.setString(i++, objeto.getNomePais());
            stmt.setString(i, objeto.getSiglaPais());

            stmt.execute();

            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            objeto.setIdPais(rs.getInt(1));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

	public void alterarPais(Pais objeto, Connection connection) {
        try {
            String sql =
                    "UPDATE Pais " +
					"SET " +
						"nomePais = ?, " +
						"siglaPais = ? " +
					"WHERE idPais = ?;";

            PreparedStatement stmt = connection.prepareStatement(sql);

			int i = 1;
            stmt.setString(i++, objeto.getNomePais());
            stmt.setString(i++, objeto.getSiglaPais());
            stmt.setInt(i, objeto.getIdPais());

            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

	public void deletarPais(Pais objeto, Connection connection) {
        try {
            String sql =
                    "DELETE FROM Pais " +
					"WHERE idPais = ?;";

            PreparedStatement stmt = connection.prepareStatement(sql);

            stmt.setInt(1, objeto.getIdPais());

            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet buscarPaisPorID(int idPais, Connection connection) throws EnderecoException {
        String sql =
                "SELECT * " +
                "FROM Pais " +
                "WHERE idPais = ?";

        try {
            return new Conector().buscarDados(sql, connection, idPais);
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new EnderecoException("ID não existente");
    }
    
    public ResultSet buscarTodosPais(Connection connection) throws EnderecoException {
        String sql =
                "SELECT * " +
                "FROM Pais";
        try {
            return new Conector().buscarDados(sql, connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new EnderecoException("Erro na busca de todos países");
    }

}
