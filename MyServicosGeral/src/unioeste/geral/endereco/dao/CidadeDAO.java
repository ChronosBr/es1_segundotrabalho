package unioeste.geral.endereco.dao;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.endereco.Cidade;
import unioeste.geral.endereco.exception.EnderecoException;

import java.sql.*;

public class CidadeDAO {

    public void inserirCidade(Cidade objeto, Connection connection) {
        try {
            String sql =
                    "INSERT INTO Cidade (" +
                        "nomeCidade," +
                        "UnidadeFederativa_idUnidadeFederativa," +
                        "UnidadeFederativa_Pais_idPais" +
                    ") VALUES (?,?,?)";

            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			int i = 1;
            stmt.setString(i++, objeto.getNomeCidade());
            stmt.setInt(i++, objeto.getUnidadeFederativa().getIdUnidadeFederativa());
            stmt.setInt(i, objeto.getUnidadeFederativa().getPais().getIdPais());

            stmt.execute();

            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            objeto.setIdCidade(rs.getInt(1));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

	public void alterarCidade(Cidade objeto, Connection connection) {
        try {
            String sql =
                    "UPDATE Cidade " +
					"SET " +
						"nomeCidade = ? " +
                     
					"WHERE idCidade = ?;";

            PreparedStatement stmt = connection.prepareStatement(sql);

			int i = 1;
            stmt.setString(i++, objeto.getNomeCidade());
            stmt.setInt(i, objeto.getIdCidade());

            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

	public void deletarCidade(Cidade objeto, Connection connection) {
        try {
            String sql =
                    "DELETE FROM Cidade " +
					"WHERE idCidade = ?;";

            PreparedStatement stmt = connection.prepareStatement(sql);

            stmt.setInt(1, objeto.getIdCidade());

            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet buscarCidadePorID(int idCidade, Connection connection) throws EnderecoException {
        String sql =
                "SELECT * " +
                "FROM Cidade " +
                "WHERE idCidade = ?";

        try {
            return new Conector().buscarDados(sql, connection, idCidade);
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new EnderecoException("ID não existente");
    }
    
    public ResultSet buscarCidade( Connection connection) throws EnderecoException {
        String sql =
                "SELECT * " +
                "FROM Cidade " ;

        try {
            return new Conector().buscarDados(sql, connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new EnderecoException("Não existe cidades na base de dados");
    }

}
