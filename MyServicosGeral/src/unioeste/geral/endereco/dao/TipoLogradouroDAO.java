package unioeste.geral.endereco.dao;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.endereco.TipoLogradouro;
import unioeste.geral.endereco.exception.EnderecoException;

import java.sql.*;

public class TipoLogradouroDAO {

    public void inserirTipoLogradouro(TipoLogradouro objeto, Connection connection) {
        try {
            String sql =
                    "INSERT INTO TipoLogradouro (" +
                        "nomeTipoLogradouro" +
                    ") VALUES (?)";

            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            stmt.setString(1, objeto.getNomeTipoLogradouro());

            stmt.execute();

            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            objeto.setIdTipoLogradouro(rs.getInt(1));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

	public void alterarTipoLogradouro(TipoLogradouro objeto, Connection connection) {
        try {
            String sql =
                    "UPDATE TipoLogradouro " +
					"SET " +
						"nomeTipoLogradouro = ? " +
					"WHERE idTipoLogradouro = ?;";

            PreparedStatement stmt = connection.prepareStatement(sql);

			int i = 1;
            stmt.setString(i++, objeto.getNomeTipoLogradouro());
            stmt.setInt(i, objeto.getIdTipoLogradouro());

            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

	public void deletarTipoLogradouro(TipoLogradouro objeto, Connection connection) {
        try {
            String sql =
                    "DELETE FROM TipoLogradouro " +
					"WHERE idTipoLogradouro = ?;";

            PreparedStatement stmt = connection.prepareStatement(sql);

            stmt.setInt(1, objeto.getIdTipoLogradouro());

            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet buscarTipoLogradouroPorID(int idTipoLogradouro, Connection connection) throws EnderecoException {
        String sql =
                "SELECT * " +
                "FROM TipoLogradouro " +
                "WHERE idTipoLogradouro = ?";

        try {
            return new Conector().buscarDados(sql, connection, idTipoLogradouro);
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new EnderecoException("ID não existente");
    }
    
    public ResultSet buscarTipoLogradouro(Connection connection) throws EnderecoException {
        String sql =
                "SELECT * " +
                "FROM TipoLogradouro ";

        try {
            return new Conector().buscarDados(sql, connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new EnderecoException("Não existente tipo logradouro cadastrado no sistema");
    }

}
